PairPipe = {}
PairPipe.__index = PairPipe

function PairPipe:create(positionX, velocityX, state)
    local pair_pipe = {}
    setmetatable(pair_pipe, PairPipe)
    pair_pipe.positionX = positionX
    pair_pipe.spaceY = love.math.random(pipeSpaceYMin, pipeSpaceYMax) -- позиция по Y пустого пространства между верхней и нижней трубой.
    pair_pipe.velocityX = velocityX
    -- print(pair_pipe.spaceY)
    pair_pipe.state = state or 0
    return pair_pipe
end

function PairPipe:update()
    self.positionX = self.positionX + self.velocityX
end

function PairPipe:draw()
    love.graphics.draw(pipe_image, self.positionX, self.spaceY + pipeSpaceSize)
    love.graphics.draw(pipe_image, self.positionX, self.spaceY, 0,1,-1)
end

function PairPipe:isPassed() -- труба скрылась из виду.
    return (self.positionX <= -pipe_image:getWidth())
end