PipesManager = {}
PipesManager.__index = PipesManager

function PipesManager:create(n, pipes_velocity)
    local manager = {}
    setmetatable(manager, PipesManager)
    manager.n = n or 2
    manager.pipe_pairs = {}
    manager.origin = width / 3
    manager.pipes_velocity = pipes_velocity or 10

    for i = 1, manager.n do
        if i == 1 then
            manager.pipe_pairs[1] = PairPipe:create(manager.origin + pipe_distX * (i-1), pipes_velocity)
            manager.pipe_pairs[1].spaceY = 200
        else 
            manager.pipe_pairs[i] = PairPipe:create(manager.origin + pipe_distX * (i-1), pipes_velocity)
        end
    end
    return manager
end

function PipesManager:update(dt)
    if #self.pipe_pairs < self.n then
        self.pipe_pairs[#self.pipe_pairs + 1] = PairPipe:create(self.origin + pipe_distX * (#self.pipe_pairs-1), self.pipes_velocity)
    end

    for k, v in pairs(self.pipe_pairs) do
        if v:isPassed() then
            v = PairPipe:create(self.origin + pipe_distX * (#self.pipe_pairs-1), self.pipes_velocity)
            self.pipe_pairs[k] = v
        end
        v:update()
    end
end

function PipesManager:draw()
    for i = 1, 2 do
        self.pipe_pairs[i]:draw()
    end
end