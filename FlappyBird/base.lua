Base = {}
Base.__index = Base

function Base:create(image, start_pos, velocity)
    local base = {}
    setmetatable(base, Base)
    base.image = image
    base.velocity = velocity or Vector:create(2, 0)

    base.part_one_pos = start_pos:copy()
    base.part_two_pos = start_pos:copy()
    base.part_two_pos.x = base.part_two_pos.x + image:getWidth()
    return base
end

function Base:update()
    if self.part_one_pos.x + self.image:getWidth() <= 0 then
        self.part_one_pos.x = self.part_two_pos.x + self.image:getWidth() 

    elseif self.part_two_pos.x + self.image:getWidth()<= 0 then
            self.part_two_pos.x = self.part_one_pos.x + self.image:getWidth()
    end
    self.part_one_pos = self.part_one_pos - self.velocity
    self.part_two_pos = self.part_two_pos - self.velocity
    
end

function Base:draw()
    love.graphics.draw(self.image, self.part_one_pos.x, self.part_one_pos.y)
    love.graphics.draw(self.image, self.part_two_pos.x, self.part_two_pos.y)
end

