Bird = {}
Bird.__index = Bird

function Bird:create(position, velocity, mass)
    local bird = {}
    setmetatable(bird, Bird)
    bird.image = image
    bird.position = position
    bird.velocity = velocity
    bird.mass = mass or 1
    bird.state = "ok"    -- collision - птичка столкнулась с трубой, fell - птичка упала на землю, ok - летит
    bird.accel = Vector:create(0, 0)
    self.wings_pos = 0
    return bird
end

function Bird:draw()
    love.graphics.draw(bird_img[self.wings_pos], self.position.x, self.position.y)
end

function Bird:update(dt)
    self.velocity.y = self.velocity.y + g
    if self.velocity.y <= 0 then
        self.position.y = self.position.y + (self.velocity.y * dt)
    else
        self.position.y = self.position.y + (self.velocity.y / 1.5 * dt)
    end
    self.velocity.y = self.velocity.y + g

    if self.state == "ok" then
        delay = delay + 1

        if delay >= 7 then
            if self.wings_pos < 2 then
                self.wings_pos = self.wings_pos + 1
            else
                self.wings_pos = 0
            end
            delay = 0
        end

        for i=1, #PM.pipe_pairs do     
            if PM.pipe_pairs[i].positionX + pipe_image:getWidth() < self.position.x and PM.pipe_pairs[i].state == 0 then
                score = score + 1
                print("Score", score)
                PM.pipe_pairs[i].state = 1
            end
        end 
    elseif self.state == "collision" then
        self.velocity.x = 0
        if self.position.y > height then
            game_state = "game_over"
        end
    end
end

function Bird:checkIntersection(pipes)
    -- local x1 = pipes.positionX
    -- local y1 = pipes.spaceY + pipeSpaceSize

    -- local x2 = pipes.positionX + pipe_image:getWidth()
    -- local y2 = pipes.spaceY + pipeSpaceSize

    -- local x3 = self.position.x
    -- local y3 = self.position.x + bird_img[self.state]:getHeight()

    -- local x4 = self.position.x + bird_img[self.state]:getWidth()
    -- local y4 = self.position.x

    -- local left = math.max(x1, x3);
    -- local top = math.min(y2, y4);
    -- local right = math.min(x2, x4);
    -- local bottom = math.max(y1, y3);

    -- local width = right - left;
    -- local height = top - bottom;
    -- -- print(width, height)
    -- if width < 0 or height < 0 then
    --     return false
    -- end

    -- return true
    local right = self.position.x + bird_img[self.wings_pos]:getWidth() - 15
    local pipe_down = pipes.spaceY + pipeSpaceSize
    local pipe_up = pipes.spaceY 
    if right > pipes.positionX and self.position.x < (pipes.positionX + bird_img[self.wings_pos]:getWidth()) and
        (self.position.y + 25 < pipe_up or self.position.y + bird_img[self.wings_pos]:getHeight() + 35 > pipe_down) then
        return true
    end
end

function Bird:checkBoundaries()
    
end

