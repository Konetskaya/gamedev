function love.conf(t)
    t.console = true
    t.window.width = 550
    t.window.height = 768
    t.window.msaa = 2
    t.window.title = "Flappy Bird"
end