require "Vector"
require "Bird"
require "Base"
require "PipesManager"
require "PairPipe"

function love.load()
    math.randomseed(os.time())
    
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()

    g = 10
    game_state = "start"
    score = 0

    start_screen = nil
    gameover_screen = nil

    pipeSpaceSize = 100
    pipeSpaceYMin = 100 --75
    pipeSpaceYMax = 250 -- 225
    pipe_distX = 250
    pipes_velocity = -2


    game_score = {0, 0}
    bird_img = {}
    delay = 0
    pipe_image = nil

    loadSprites()
    loadAudio()
    scaleX = width / background_day:getWidth()
    scaleY = height / background_day:getHeight()
    
    bird_scale = 1.8

    base = nil
    bird = nil
    PM = nil

end


function love.update(dt)
    if game_state == "game" then
        if bird.state == "ok" then
            base:update()
            PM:update()
        elseif bird.state == "ok" then
        end
        bird:update(dt)
        for i=1, #PM.pipe_pairs do
            if bird:checkIntersection(PM.pipe_pairs[i]) then
                bird.state = "collision"
            end
        end
    end
end

function love.draw()
    if game_state == "start" then
        -- love.graphics.push()
        -- love.graphics.scale(scaleX, scaleY)
        -- love.graphics.pop()
        love.graphics.scale(2.5, 2.5)
        love.graphics.draw(start_screen, 20, 20)

        
    elseif game_state == "game" then
        love.graphics.push()
        love.graphics.scale(scaleX, scaleY)
        love.graphics.draw(background_day, 0, 0)
        PM:draw()
        base:draw()
        love.graphics.pop()

        love.graphics.scale(bird_scale, bird_scale)
        bird:draw()
    elseif game_state == "game_over" then
        love.graphics.draw(gameover_screen, width / 3, height / 3)
    end

end

function love.keypressed(key)
    if game_state == "start" then
        game_state = "game" 
        base = Base:create(base_image, Vector:create(0, height / scaleY - base_image:getHeight()))
        bird = Bird:create(Vector:create((width / 2) / bird_scale, (height / 2)) / bird_scale, Vector:create(0, 20))
        PM = PipesManager:create(2, pipes_velocity) 
    elseif game_state == "game_over" then
        score = 0
        if key == "space" then
            game_state = "start"
        end
    elseif game_state == "game" then
        if key == "space"  and bird.state == "ok" then
            bird.velocity.y = -200
        end
    end
end


function loadSprites()
    numbers = {}
    for i = 0, 9 do
        numbers[i] = love.graphics.newImage("assets/sprites/" .. tostring(i) .. ".png")
    end

    background_day = love.graphics.newImage("assets/sprites/background-day.png")
    background_night = love.graphics.newImage("assets/sprites/background-night.png")

    base_image = love.graphics.newImage("assets/sprites/base.png")

    bird_img[0] = love.graphics.newImage("assets/sprites/redbird-downflap.png")
    bird_img[1] = love.graphics.newImage("assets/sprites/redbird-midflap.png")
    bird_img[2] = love.graphics.newImage("assets/sprites/redbird-upflap.png")

    pipe_image = love.graphics.newImage("assets/sprites/pipe-green.png")

    start_screen = love.graphics.newImage("assets/sprites/message.png")
    gameover_screen = love.graphics.newImage("assets/sprites/gameover.png")
end

function loadAudio()
    die_audio = love.audio.newSource("assets/audio/die.wav", "static")
    hit_audio = love.audio.newSource("assets/audio/hit.wav", "static")
    point_audio = love.audio.newSource("assets/audio/point.wav", "static")
    swoosh_audio = love.audio.newSource("assets/audio/swoosh.wav", "static")
    wig_audio = love.audio.newSource("assets/audio/wing.wav", "static")
end