FlowField = {}
FlowField.__index = FlowField

function FlowField:create(resolution)
    local flow = {}
    setmetatable(flow, FlowField)
    flow.field = {}
    flow.resolution = resolution
    love.math.setRandomSeed(10000)
    flow.angle = 0
    return flow
end

function FlowField:init()
    local cols = width / self.resolution
    local rows = height / self.resolution

    for i = 0, cols do
        self.field[i] = {}
        self.angle = 0
        for j = 0, rows do
            -- self.angle =  self.angle + 0.1
            -- self.field[i][j] = Vector:create(1, self.angle)
            self.field[i][j] = Vector:create(1, 0.2)
        end
    end
end

function FlowField:lookup(v)
 
    local col = math.floor(v.x / self.resolution)
    local row = math.floor(v.y / self.resolution)

    col = math.constrain(col, 0, #self.field)
    row = math.constrain(row, 0, #self.field[0])
    return self.field[col][row]:copy()
end

function FlowField:draw()
    r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(1, 1, 1, 0.3)
    for i = 0, #self.field do
        for j = 0, #self.field[i] do
            drawVector(self.field[i][j], i * self.resolution, j * self.resolution, self.resolution - 2)
        end
    end
    love.graphics.setColor(r, g, b, a)
end

function drawVector(v, x, y, scale)
    love.graphics.push()
    love.graphics.translate(x, y)
    love.graphics.rotate(v:heading())
    local len = v:mag() * scale
    love.graphics.line(0, 0, len, 0)
    love.graphics.pop()
end