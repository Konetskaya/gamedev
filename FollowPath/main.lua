require("Vector")
require("Vehicle")
require("Flow")
require("Path")

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()

    debug = true

    vehicle1 = Vehicle:create(0, 300)
    vehicle1.velocity.x = 2

    path_points = {{Vector:create(0, 300), Vector:create(250, 300)},
                {Vector:create(250, 300), Vector:create(450, 500)}, 
                {Vector:create(450, 500), Vector:create(width, 500)},}
    prev_line = 0

    path = Path:create(path_points)
end

function love.update(dt)
    vehicle1:borders()
    vehicle1:update(path)

    -- print(love.mouse.getPosition())
end

function love.draw()
    path:draw()
    vehicle1:draw()
end


