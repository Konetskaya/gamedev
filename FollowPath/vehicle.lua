Vehicle = {}
Vehicle.__index = Vehicle

function Vehicle:create(x, y) 
    local vehicle = {}
    setmetatable(vehicle, Vehicle)

    vehicle.position = Vector:create(x, y)
    vehicle.velocity = Vector:create(0, 0)
    vehicle.acceleration = Vector:create(0, 0)

    vehicle.r = 10
    vehicle.vertices = {0, -vehicle.r * 2, 
        -vehicle.r, vehicle.r * 2, 
        vehicle.r, vehicle.r * 2}

    vehicle.maxSpeed = 4
    vehicle.maxForce = 0.1
    vehicle.wtheta = 0
    vehicle.target = Vector:create(0, 0)

    vehicle.line_n = 0
    return vehicle
end

function Vehicle:update(path)
    self:follow(path)
    self.velocity:add(self.acceleration)
    self.velocity:limit(self.maxSpeed)

    self.position:add(self.velocity)
    self.acceleration:mul(0)
end

function Vehicle:applyForce(force)
    self.acceleration:add(force)
end

function Vehicle:follow(path)
    local predict = self.velocity:copy()
    predict:norm()
    predict:mul(50)

    local pos = self.position + predict
    local distance = 5000
    local start_line = true
    local end_line = false
    local n = 0
    for i=1, #path.points do
        local a = path.points[i][1]
        local b = path.points[i][2]
  
        local normal = getNormal(pos, a, b)
        
        local dir = b - a
        dir:norm()
        dir:mul(10)

        local target = normal + dir
        d = pos:distTo(normal)    

        if self.line_n == #path.points and onPath(target, a, b) == false then
            if a:distTo(b) <= a:distTo(target) then
                local a = path.points[1][1]
                local b = path.points[1][2]
          
                local normal = getNormal(pos, a, b)
                
                local dir = b - a
                dir:norm()
                dir:mul(10)
                distance = pos:distTo(normal) 
                self.target = normal + dir
                n = 1
            else 
                n = i
            end
        elseif d < distance and onPath(normal, a, b) then
            distance = d
            self.target = normal + dir
            n = i
        end        
    end

    if distance > path.d then
        self:seek(self.target)
        self.line_n = n
    end
end

function Vehicle:seek(target)
    local desired = target - self.position
    
    if desired:mag() == 0 then
        return
    end

    desired:norm()
    desired:mul(self.maxSpeed)
    local steer = desired - self.velocity
    steer:limit(self.maxForce)
    self:applyForce(steer)
end

function Vehicle:wander()
    local rwander = 25
    local dwander = 90

    self.wtheta = self.wtheta + love.math.random(-30, 30) / 100
    local pos = self.velocity:copy() 

    pos:norm()
    pos:mul(dwander)
    pos:add(self.position)

    local h =  self.velocity:heading()

    local offset = Vector:create(rwander * math.cos(self.wtheta + h), rwander * math.sin(self.wtheta + h))
    local target = pos + offset
    self:seek(target)

    love.graphics.circle("line", pos.x, pos.y, rwander)
    love.graphics.circle("fill", target.x, target.y, 4)
end

function Vehicle:borders()
    if self.position.x < -self.r then
        self.position.x = width + self.r
    end
    if self.position.y < -self.r then
        self.position.y = height + self.r
    end

    if self.position.x > width + self.r then
        self.position.x = -self.r
    end
    if self.position.y > height + self.r then
        self.position.y = -self.r
    end
end


function Vehicle:boundaries()
    local desired = nil

    if self.position.x < d then
        desired = Vector:create(self.maxSpeed, self.velocity.y)
    elseif self.position.x > width - d then
        desired = Vector:create(-self.maxSpeed, self.velocity.y)
    end

    if self.position.y < d then
        desired = Vector:create(self.velocity.x, self.maxSpeed)
    elseif self.position.y > height - d then
        desired = Vector:create(self.velocity.x, -self.maxSpeed)
    end

    if desired then
        desired:norm()
        desired:mul(self.maxSpeed)
        local steer = desired - self.velocity
        steer:limit(self.maxForce)
        self:applyForce(steer)
    end
end

function Vehicle:draw()
    local theta = self.velocity:heading()
    theta = theta + 90 * 3.14 / 180
    love.graphics.push()
    love.graphics.translate(self.position.x, self.position.y)
    love.graphics.rotate(theta)
    love.graphics.polygon("fill", self.vertices)
    love.graphics.pop()
    love.graphics.circle("fill", self.target.x, self.target.y, 4)
end

function getNormal(p, a, b)
    local ap = p - a
    local ab = b - a
    ab:norm()
    ab:mul(ap:dot(ab))
    local point = a + ab
    return point
end

function onPath(n, a, b)
    local d1 = n:distTo(a)
    local d2 = n:distTo(b)
    return (d1 + d2) == a:distTo(b)
end