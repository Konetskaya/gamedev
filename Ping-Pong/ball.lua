Ball = {}
Ball.__index = Ball

function Ball:create(location, velocity)
    local ball = {}
    setmetatable(ball, Ball)
    ball.velocity = velocity
    ball.location = location
    ball.size = 10
    ball.sound = love.audio.newSource("resourses/pong.wav", "static")
    return ball
end

function Ball:draw()
    love.graphics.rectangle("fill", self.location.x, self.location.y, self.size, self.size)
end

function Ball:update()
    self.location = self.location + self.velocity
end

function Ball:check_boundaries(left, right, top, bottom)
    if self.location.x <= left or self.location.x >= right - self.size then
        self.velocity.x = self.velocity.x * -1
        self.sound:play()
    end

    if self.location.y <= top or self.location.y >= bottom - self.size then
        self.velocity.y = self.velocity.y * -1
        self.sound:play()
    end

end

