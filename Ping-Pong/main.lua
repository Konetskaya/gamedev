require("Vector")
require("Ball")
require("Platform")

function love.load()
    state = "start"
    font_name = "resourses/AtariClassic-Regular.ttf"
    font_small = love.graphics.newFont(font_name, 10)
    font_big = love.graphics.newFont(font_name, 24)

    p1_score = 0
    p2_score = 0
    
    max_points = 9
    ball = Ball:create(Vector:create(395, 285), Vector:create(0, 0))

    p1_platform = Platform:create(Vector:create(20, 285), Vector:create(0, 0), 40)
    p2_platform = Platform:create(Vector:create(760, 285), Vector:create(0, 0), 40, "auto")

    m_ball = Vector:create(0, ball.size / 2)
    m_platform = Vector:create(0, p2_platform.height / 2)

    gameover_sound = love.audio.newSource("resourses/gameover.wav", "static")
    win_sound = love.audio.newSource("resourses/victory.wav", "static")
end

function love.draw()
    if state == "start" then
        love.graphics.setFont(font_big)
        love.graphics.print("PONG GAME", 280, 250)
        love.graphics.print("Press any key to start..", 120, 300)

    elseif state == "game" then
        love.graphics.line(400, 0, 400, 600)
        love.graphics.rectangle("line", 30, 60, 740, 500)
        love.graphics.setFont(font_big)
        love.graphics.print(tostring(p1_score), 350, 20)
        love.graphics.print(tostring(p2_score), 430, 20)

        love.graphics.setFont(font_small)
        love.graphics.print("FPS: " .. tostring(love.timer.getFPS()), 700, 580)

        ball:draw()
        p1_platform:draw()
        p2_platform:draw()

    elseif state == "gameover" then
        love.graphics.setFont(font_big)
        love.graphics.print("GAME OVER", 280, 270)

    elseif state == "win" then
        love.graphics.setFont(font_big)
        love.graphics.print("YOU WIN!", 280, 270)
    end  
end


function love.update()
    if state == "game" then
        ball:update()
        ball:check_boundaries(30, 770, 60, 560)

        p1_platform:update()
        p1_platform:check_boundaries(500 + p1_platform.height / 2, 60)

    
        p1_up = p1_platform.location.y
        p1_down = p1_platform.location.y + p1_platform.height

        p2_up = p2_platform.location.y
        p2_down = p2_platform.location.y + p2_platform.height

        if ball.location.x <=  30 and (ball.location.y < p1_up or ball.location.y > p1_down) then
            p2_score = p2_score + 1
            ball.velocity = ball.velocity + ball.velocity * (0.1)
        end

        if ball.location.x >=  760 and (ball.location.y < p2_up or ball.location.y  > p2_down) then
            p1_score = p1_score + 1
            ball.velocity = ball.velocity + ball.velocity * (0.1)
        end

        if p2_platform.mode == "auto" then
            p2_platform:autoMode(ball.location, m_ball, m_platform)
            p2_platform:update()
        end

        if p1_score == max_points then
            state = "win"
            win_sound:play()
        elseif p2_score == max_points then
            state = "gameover"
            gameover_sound:play()
        end
    end
end

function love.keypressed(key)
    if state == "start" then
        p1_platform.location = Vector:create(20, 285)
        p2_platform.location = Vector:create(760, 285)
        p1_platform.velocity = Vector:create(0, 0)
        p2_platform.velocity = Vector:create(0, 0)
        ball.location = Vector:create(395, 285)
        ball.velocity = Vector:random(-3, 3, -3, 3)
        state = "game"
    elseif state == "gameover" or state == "win" then
        p1_score = 0
        p2_score = 0
        state = "start"
    elseif state == "game" then
        if key == "up" then
            p1_platform.velocity.y = -4

        elseif key == "down" then
            p1_platform.velocity.y = 4
        end
    end
end
