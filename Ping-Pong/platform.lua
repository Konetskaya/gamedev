Platform = {}
Platform.__index = Platform

function Platform:create(location, velocity, height, mode)
    local platform = {}
    setmetatable(platform, Platform)
    platform.location = location
    platform.velocity = velocity
    platform.height = height
    platform.width = 20
    platform.mode = mode or "manual"
    return platform
end

function Platform:draw()
    love.graphics.rectangle("fill", self.location.x, self.location.y, self.width, self.height)
end

function Platform:update()
    self.location = self.location + self.velocity
end

function Platform:check_boundaries(up, down)
    if self.location.y > up then
        self.location.y = up
    elseif self.location.y < down then
        self.location.y = down
    end
end

function Platform:autoMode(ball_location, m_ball, m_platform)
    local direction = ball_location - m_ball - self.location - m_platform
    local acceleration = direction:norm() * 5
    self.velocity.y = acceleration.y
end