Particle = {}
Particle.__index = Particle


function Particle:create(x, y)
    local particle = {}
    setmetatable(particle, Particle)

    particle.location = Vector:create(x, y)
    particle.velocity = Vector:create(math.random(-150, 0) / 100, math.random(-5, 5) / 100)

    particle.acceleration = Vector:create(0, 0.05)
    particle.lifespan = math.random(10, 20)
    particle.decay = math.random(3, 10) / 10
    return particle
end

function Particle:update()
    self.velocity:add(self.acceleration)
    self.location:add(self.velocity)
    self.acceleration:mul(0)
    self.lifespan = self.lifespan - self.decay
end

function Particle:applyForce(force)
    self.acceleration:add(force)
end

function Particle:isDead()
    return self.lifespan < 0
end

function Particle:draw()
    r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(250 / 255, 150 /255, 20  /255, self.lifespan / 100)
    love.graphics.draw(textures.luminescence, self.location.x, self.location.y, 0.6, 0.6)
    love.graphics.setColor(r, g, b, a)
end