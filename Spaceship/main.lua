require("Vector")
require("Mover")
require("ParticleSystem")
require("Particle")

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()
    textures = {}
    textures.luminescence = love.graphics.newImage("assets/texture.png")

    local location = Vector:create(width/2, height/2)
    velocity = Vector:create(0, 0)
    mover = Mover:create(location, velocity)

    systemL = ParticleSystem:create(width / 2, height - height/3, 100)
    systemR = ParticleSystem:create(width / 2, height - height/3, 100)
end

function love.update()
    if love.keyboard.isDown("left") then
        mover.angle = mover.angle - 0.05
    end
    if love.keyboard.isDown("right") then
        mover.angle = mover.angle + 0.05
    end

    -- print("Angle", mover.angle)
    if love.keyboard.isDown("up") then
        local x = math.cos(mover.angle)
        local y = math.sin(mover.angle)
        mover:applyForce(Vector:create(x, y))
        mover.active = true
    else

        mover.active = false
    end
    mover:update()
    mover:check_boundaries()
    mover.velocity = mover.velocity:limit(5)
    systemL:update()
    systemR:update()
end

function love.draw()
    mover:draw()
    -- -- systemL:draw()
    -- if mover.active then
    --     systemL:draw()
    -- end
end


