Mover = {}
Mover.__index = Mover

function Mover:create(location, velocity, mass)
    local mover = {}
    setmetatable(mover, Mover)
    mover.location = location
    mover.velocity = velocity

    mover.acceleration = Vector:create(0, 0)
    mover.mass = mass or 1
    
    mover.size = 20 * mover.mass
    mover.aAcceleration = 0 -- угловое ускорение.
    mover.aVelocity = 0  -- угловая скорость.
    mover.angle = 0
    mover.active = false
    return mover
end

function Mover:random()
    local location = Vector:create()
    location.x = love.math.random(0, love.graphics.getWidth())
    location.y = love.math.random(0, love.graphics.getHeight())

    local velocity = Vector:create()
    velocity.x = love.math.random(-2, 2)
    velocity.y = love.math.random(-2, 2)

    return Mover:create(location, velocity)
end

function Mover:draw()
    love.graphics.push()
    
    love.graphics.translate(self.location.x, self.location.y)
    love.graphics.rotate(self.angle)

    r, g, b, a = love.graphics.getColor()
    love.graphics.line(30, 0, -30, 30, -30, -30, 30, 0)
    type = "line"

    if self.active then
        love.graphics.setColor(1, 0, 0, 1)
        type = "fill"
    end
    systemL.origin = Vector:create(-40 - 10, -30)
    systemR.origin = Vector:create(-40 - 10, 0)

    love.graphics.rectangle(type, -40, -25, 10, 20)
    love.graphics.rectangle(type, -40, 5, 10, 20)

    love.graphics.setColor(r, g, b, a)
    if self.active then
        systemL:draw()
        systemR:draw()
    end
    love.graphics.pop()
end

function Mover:update()
    self.velocity:add(self.acceleration)
    self.location:add(self.velocity)
    mover.aVelocity = self.aVelocity + self.aAcceleration
    self.acceleration:mul(0)
end

function Mover:check_boundaries()
    if self.location.x > width - self.size then
        self.location.x = width - self.size
        self.velocity.x = 0
        self.acceleration.x = -2

    elseif self.location.x < self.size then
        self.location.x = self.size
        self.velocity.x = 0
        self.acceleration.x = 2
    end

    if self.location.y > height - self.size then
        self.location.y = height - self.size
        self.velocity.y = self.velocity.y * -1

    elseif self.location.y < self.size then
        self.location.y = self.size
        self.velocity.y = self.velocity.y
    end
end

function Mover:applyForce(force)
    self.acceleration:add(force / self.mass)    
end


