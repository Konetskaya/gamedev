FlowField = {}
FlowField.__index = FlowField

function FlowField:create(resolution)
    local flow = {}
    setmetatable(flow, FlowField)
    flow.field = {}
    flow.resolution = resolution
    love.math.setRandomSeed(10000)
    return flow
end

function FlowField:init()
    local cols = width / self.resolution
    local rows = height / self.resolution

    local dSafe = 700
    local a = -0.1

    for i = 0, cols do
        self.field[i] = {}
        for j = 0, rows do
                local v = Vector:create(i * self.resolution, j * self.resolution) - c

                -- theta = theta + step
                -- r = a + b*theta
                -- x = r * math.cos(theta) + c.x
                -- y = r * math.sin(theta) + c.y

                -- self.field[i][j] = v

                -- table.insert(points, Vector:create(x, y))

                -- v = v + t
            
                -- v:norm()

                -- double r = sqrt(x*x + y*y);
                -- double tx = a*x + r*y;
                -- double ty = a*y - r*x;
    
                local r = math.sqrt(v.x * v.x + v.y * v.y) 
                local t = Vector:create(a * v.x - r * v.y, a * v.y + r * v.x)
                local tLen = math.sqrt(t.x * t.x + t.y * t.y)
                local k = dSafe / tLen;
                t:mul(k)
    
                table.insert(points, t)
                v = v + t
                
                v:norm()
                v:mul(-1)
    
               self.field[i][j] = v
                
        end
    end
end

function FlowField:lookup(v)
 
    local col = math.floor(v.x / self.resolution)
    local row = math.floor(v.y / self.resolution)
    -- print(col, row)
    col = math.constrain(col, 0, #self.field)
    row = math.constrain(row, 0, #self.field[0])
    return self.field[col][row]:copy()
end

function FlowField:draw()
    r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(1, 1, 1, 0.3)
    for i = 0, #self.field do
        for j = 0, #self.field[i] do
            drawVector(self.field[i][j], i * self.resolution, j * self.resolution, self.resolution - 2)
        end
    end
    love.graphics.setColor(r, g, b, a)
end

function drawVector(v, x, y, scale)
    love.graphics.push()
    love.graphics.translate(x, y)
    love.graphics.rotate(v:heading())
    local len = v:mag() * scale
    love.graphics.line(0, 0, len, 0)
    love.graphics.pop()
end