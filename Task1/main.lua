require("Vector")
require("Vehicle")
require("Flow")

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()

    c = Vector:create(width /2, height / 2)
    local cols = width / 80
    local rows = height / 80

    a = 1
    b = 25
    step = 0.1
    -- step = 360 / (cols * height)
    -- print(step)

    loops = 4

    theta = 10.0
    r = a

    prev_x = r * math.cos(theta)
    prev_y = r * math.sin(theta)
    x = 0
    y = 0

    points = {}
    debug = true
    vehicles = {}

    for i = 0, 51 do
        vehicles[i] = Vehicle:create(math.random(0, width), math.random(0, height))
        vehicles[i].velocity.x = 5
    end

    resolution = 80
    flow = FlowField:create(resolution)
    flow:init()


    -- print(points[#points])
end

function love.update(dt) -- dt - разница между кадрами
    for i = 0, #vehicles do
        vehicles[i]:borders()
        vehicles[i]:follow(flow)
        vehicles[i]:update()
    end
end

function love.draw()
    for i = 0, #vehicles do
        vehicles[i]:draw()
    end
    for i = 1, #points do
        love.graphics.circle("fill", points[i].x, points[i].y, 2)
    end
    flow:draw()
end




                -- theta = theta + step
                -- r = a + b*theta
                -- x = r * math.cos(theta) + c.x
                -- y = r * math.sin(theta) + c.y

                -- self.field[i][j] = v

                -- table.insert(points, Vector:create(x, y))
            -- end