Path = {}
Path.__index = Path

function Path:create(points, d)
    local path = {}
    setmetatable(path, Path)
    path.points = points
    path.d = 1
    path.start = Vector:create(math.ceil(points[1][1].x), math.ceil(points[1][2].y))
    path.stop = Vector:create(math.ceil(points[#points][1].x), math.ceil(points[#points][2].y))
    return path
end

function Path:draw()
    local r, g, b, a = love.graphics.getColor()
    local blend = love.graphics.getBlendMode()
    love.graphics.setLineWidth(self.d)
    love.graphics.setColor(0.31, 0.31, 0.31, 0.7)
    
    for i=1, #self.points do
        love.graphics.line(self.points[i][1].x, self.points[i][1].y, self.points[i][2].x, self.points[i][2].y)
    end

    love.graphics.setBlendMode("replace")

    for i=1, #self.points do
        love.graphics.circle("fill", self.points[i][1].x, self.points[i][1].y, self.d / 2)
    end


    love.graphics.setColor(r, g, b, a)
    love.graphics.setBlendMode(blend)
end