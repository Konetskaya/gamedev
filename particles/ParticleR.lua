ParticleR = {}
ParticleR.__index = ParticleR


function ParticleR:create(x, y, size)
    local particle = {}
    setmetatable(particle, ParticleR)

    particle.location = Vector:create(x, y)
    particle.velocity = Vector:create(math.random(-200, 200) / 100, math.random(-200, 200) / 100)

    particle.acceleration = Vector:create(0, 0.05)
    particle.lifespan = 100
    particle.decay = 0
    particle.size = size
    
    particle.clicked = false
    particle.dead = false
    return particle
end

function ParticleR:update()
    if self.clicked then
        self.lifespan = self.lifespan - self.decay
        self.location.x = self.location.x + 1
        self.location.y = self.location.y + 1
        self.size.x  = self.size.x - 1
        self.size.y  = self.size.y - 1
    end
end

function ParticleR:applyForce(force)
    self.acceleration:add(force)
end

function ParticleR:isDead()
    return self.lifespan < 0
end

function ParticleR:draw()
    r, g, b, a = love.graphics.getColor()
    if self.clicked then
        love.graphics.setColor(1, 1, 1, 1)
    else
        love.graphics.setColor(240 / 255, 7  /255, 77  /255, self.lifespan / 100)
    end
    love.graphics.rectangle("line", self.location.x, self.location.y, self.size.x, self.size.y)
    love.graphics.setColor(r, g, b, a)
end

function ParticleR:isClicked(mouse_x, mouse_y) 
    if ((mouse_x >  self.location.x) and mouse_x < (self.location.x + self.size.x)) and
    ((mouse_y >  self.location.y) and (mouse_y < self.location.y + self.size.y)) then
        self.clicked = true
        self.decay = math.random(30, 40) / 10
        self.dead = true
    else
        self.clicked = false
    end
    return self.clicked
end