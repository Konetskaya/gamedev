ParticleSystemR = {}
ParticleSystemR.__index = ParticleSystemR


function ParticleSystemR:create(x, y, n)
    local system = {}
    setmetatable(system, ParticleSystemR)

    system.origin = Vector:create(x, y)
    system.n = n or 10
    system.particles = {}
    system.index = 0
    return system
end

function ParticleSystemR:draw()
    r, g, b, a = love.graphics.getColor()
    love.graphics.circle("line", self.origin.x, self.origin.y, 5)

    for k, v in pairs(self.particles) do
        v:draw()
    end
    love.graphics.setColor(r, g, b, a)

end

function ParticleSystemR:update()
    if #self.particles < self.n then
        -- self.particles[self.index] = Particle:create(self.origin.x, self.origin.y)
        local loc = Vector:create(math.random(0, love.graphics.getWidth()), math.random(0, love.graphics.getHeight()))
        local size = Vector:create(math.random(30, 70), math.random(70, 90))

        self.particles[self.index] = ParticleR:create(loc.x, loc.y, size)
        self.index = self.index + 1
    end
    for k, v in pairs(self.particles) do
        if v:isDead() then
            -- ParticleKids:create()
            -- v = Particle:create(self.origin.x, self.origin.y)
            local loc = Vector:create(math.random(0, love.graphics.getWidth()), math.random(0, love.graphics.getHeight()))
            local size = Vector:create(math.random(30, 70), math.random(70, 90))
            v = ParticleR:create(loc.x, loc.y, size)
            self.particles[k] = v
        end
        v:update()
    end
end

function ParticleSystemR:applyForce(force)
    for k, v in pairs(self.particles) do
        v:applyForce(force)
    end
end

function ParticleSystemR:applyRepeller(repeller)
    for k, v in pairs(self.particles) do
        local force = repeller:repl(v)
        v:applyForce(force)
    end   
end