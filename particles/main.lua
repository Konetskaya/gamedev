require("Vector")
require("Particle")
require("ParticleSystemR")
require("Repeller")
require("ParticleR")

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()
    textures = {}
    textures.heart = love.graphics.newImage("assets/heart.png")
    textures.luminescence = love.graphics.newImage("assets/texture.png")

    -- wind = Vector:create(0.05, 0)
    -- gravity = Vector:create(0, 0.1)
    -- repeller = Repeller:create(width / 2 + 100, height / 2 + 150)
    system = ParticleSystemR:create(width / 2, height / 2, 20)

    particleR = ParticleR:create(width / 2, height / 2, Vector:create(math.random(30, 100), math.random(30, 100)))
    
end

function love.update()
    particleR:update()

    system:update()
    -- if particle:isDead() then
    --     particle = Particle:create(width / 2, height / 2)
    -- end
    -- system:applyForce(wind)
    -- system:applyForce(gravity)
    -- system:applyRepeller(repeller)
    -- system:update()
end

function love.draw()
    particleR:draw()
    -- repeller:draw()
    system:draw()
end

function love.mousepressed(x, y, button, istouch)
    for k, v in pairs(system.particles) do
        v:isClicked(x, y)
    end
    -- if particleR:isClicked(x, y) then
    --     print("clecked")
    -- end
end

